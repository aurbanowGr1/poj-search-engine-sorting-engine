import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import engine.CarSearchParameters;
import engine.SearchEngine;
import engine.SortingEngine;
import engine.SortingSettings;
import engine.comparers.BrandComparer;
import engine.comparers.ColumnsConstants;
import engine.filters.*;
import model.CarAdvertisement;


public class Main {

	public static void main(String[] args) {

		CarAdvertisement bmw= new CarAdvertisement();
		bmw.setBrand("BMW");
		bmw.setPrice(20000);
		bmw.setYear(2010);
		CarAdvertisement alfa = new CarAdvertisement();
		alfa.setBrand("alfa romeo");
		alfa.setPrice(15000);
		alfa.setYear(2009);
		CarAdvertisement citroen = new CarAdvertisement();
		citroen.setBrand("citroen");
		citroen.setPrice(17500);
		citroen.setYear(2013);
		ArrayList<CarAdvertisement> queryList = new ArrayList<>();
		queryList.add(citroen);
		queryList.add(alfa);
		queryList.add(bmw);
		
		SearchFilter brandFilter = new BrandSearchFilter();
		SearchFilter priceFilter = new PriceSearchFilter();
		SearchEngine engine = new SearchEngine();
		
		engine.addFilter(priceFilter);
		engine.addFilter(brandFilter);
		
		engine.setQueryList(queryList);
		
		CarSearchParameters parameters = new CarSearchParameters();
		
		engine.setParameters(parameters);
		ArrayList<CarAdvertisement> result = engine.getFilteredList();
		
		for(CarAdvertisement car : result){
			System.out.println(car.getBrand() +" "+ car.getPrice());
		}
		
		SortingEngine sortingEngine = new SortingEngine();
		sortingEngine.addComparator(ColumnsConstants.BrandComparator);
		sortingEngine.addComparator(ColumnsConstants.YearComparator);
		SortingSettings settings = new SortingSettings();
		settings.setColumnId(ColumnsConstants.YEAR_COLUMN_ID);
		sortingEngine.setSettings(settings);
		sortingEngine.setQueryList(result);
		sortingEngine.sort();

		for(CarAdvertisement car : result){
			System.out.println(car.getBrand() +" "+ car.getPrice());
		}
		
	}

}
