package model;

import java.util.Date;

public class CarAdvertisement implements Comparable<CarAdvertisement>{

	String brand;
	int price;
	int year;
	String region;
	String city;
	boolean demaged;
	boolean unused;
	Date date;
	
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public boolean isDemaged() {
		return demaged;
	}
	public void setDemaged(boolean demaged) {
		this.demaged = demaged;
	}
	public boolean isUnused() {
		return unused;
	}
	public void setUnused(boolean unused) {
		this.unused = unused;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	@Override
	public int compareTo(CarAdvertisement o) {
		return this.price - o.getPrice();
	}
	
	
}
