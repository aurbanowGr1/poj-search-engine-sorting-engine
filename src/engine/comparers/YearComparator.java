package engine.comparers;

import model.CarAdvertisement;

public class YearComparator implements ColumnComparator{

	@Override
	public int compare(CarAdvertisement o1, CarAdvertisement o2) {
		
		return o1.getYear()-o2.getYear();
	}

	@Override
	public int getColumnId() {
		return ColumnsConstants.YEAR_COLUMN_ID;
	}

}
