package engine.comparers;

import java.util.Comparator;

import model.CarAdvertisement;

public interface ColumnComparator extends Comparator<CarAdvertisement>{

	public int getColumnId();
}
