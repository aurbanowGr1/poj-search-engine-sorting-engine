package engine.comparers;

import java.util.Comparator;

import model.CarAdvertisement;

public class BrandComparer implements ColumnComparator{

	@Override
	public int compare(CarAdvertisement o1, CarAdvertisement o2) {
		
		return o1.getBrand().compareToIgnoreCase(o2.getBrand());
	}

	@Override
	public int getColumnId() {
		return ColumnsConstants.BRAND_COLUMN_ID;
	}

}
