package engine.comparers;

public class ColumnsConstants {

	public static final int BRAND_COLUMN_ID = 1;
	public static final int PRICE_COLUMN_ID = 2;
	public static final int YEAR_COLUMN_ID = 3;

	public static ColumnComparator BrandComparator = new BrandComparer();
	public static ColumnComparator YearComparator = new YearComparator();
}
