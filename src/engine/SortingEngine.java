package engine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import engine.comparers.ColumnComparator;
import model.CarAdvertisement;

public class SortingEngine {

	SortingSettings settings;
	List<CarAdvertisement> queryList;
	List<ColumnComparator> comparators = new ArrayList<ColumnComparator>();
	
	public SortingSettings getSettings() {
		return settings;
	}
	public void setSettings(SortingSettings settings) {
		this.settings = settings;
	}
	public List<CarAdvertisement> getQueryList() {
		return queryList;
	}
	public void setQueryList(List<CarAdvertisement> queryList) {
		this.queryList = queryList;
	}
	
	public void addComparator(ColumnComparator comparator){
		comparators.add(comparator);
	}
	
	public void removeComparator(ColumnComparator comparator){
		comparators.remove(comparator);
	}
	
	public void sort(){
		if(queryList==null || queryList.isEmpty())return;
		if(settings==null) return;
		for(ColumnComparator comparator: comparators){
			if(settings.getColumnId()== comparator.getColumnId()){
				Collections.sort(queryList, comparator);
				return;
			}
		}
	}
	
	
}
