package engine;

import java.util.ArrayList;

import engine.filters.SearchFilter;
import model.CarAdvertisement;

public class SearchEngine {
	
	CarSearchParameters parameters;
	ArrayList<CarAdvertisement> queryList;
	ArrayList<SearchFilter> filters = new ArrayList<SearchFilter>();
	
	public void addFilter(SearchFilter filter)
	{
		filters.add(filter);
	}
	
	public void removeFilter(SearchFilter filter)
	{
		filters.remove(filter);
	}
	
	public CarSearchParameters getParameters() {
		return parameters;
	}
	public void setParameters(CarSearchParameters parameters) {
		this.parameters = parameters;
	}
	public ArrayList<CarAdvertisement> getQueryList() {
		return queryList;
	}
	public void setQueryList(ArrayList<CarAdvertisement> queryList) {
		this.queryList = queryList;
	}

	public ArrayList<CarAdvertisement> getFilteredList(){
		
		ArrayList<CarAdvertisement> result = new ArrayList<CarAdvertisement>(queryList);
		if(queryList == null) return null;
		if(parameters == null) return queryList;
		for(SearchFilter filter: filters)
		{
			filter.setParameters(parameters);
			if(filter.canApplyFilter())
				filter.applyFilter(result);
		}
		return result;
	}
	
	
}






