package engine.filters;

import java.util.ArrayList;

import model.CarAdvertisement;
import engine.CarSearchParameters;

public class BrandSearchFilter implements SearchFilter{
	
	private CarSearchParameters parameters;
	
	
	@Override
	public CarSearchParameters getParameters() {
		return parameters;
	}

	@Override
	public void setParameters(CarSearchParameters parameters) {
		this.parameters=parameters;
	}

	@Override
	public boolean canApplyFilter() {
		return parameters.getBrand()!=null 
				&& !parameters.getBrand().isEmpty();
	}

	@Override
	public void applyFilter(ArrayList<CarAdvertisement> queryList) {
		ArrayList<CarAdvertisement> tmp = new ArrayList<CarAdvertisement>(queryList);
		for(CarAdvertisement car : tmp)
		{
			if(car.getBrand().equalsIgnoreCase(parameters.getBrand()))
				queryList.remove(car);
		}
	}

}
