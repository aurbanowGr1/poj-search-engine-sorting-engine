package engine.filters;

import java.util.ArrayList;

import model.CarAdvertisement;
import engine.CarSearchParameters;

public class PriceSearchFilter implements SearchFilter{

	CarSearchParameters parameters;
	// TODO Auto-generated method stub
	@Override
	public CarSearchParameters getParameters() {
		return parameters;
	}

	@Override
	public void setParameters(CarSearchParameters parameters) {
		
		this.parameters=parameters;
		
	}

	@Override
	public boolean canApplyFilter() {
		return parameters.getPriceFrom()>0 || parameters.getPriceTo()>0;
	}

	@Override
	public void applyFilter(ArrayList<CarAdvertisement> queryList) {
		ArrayList<CarAdvertisement> tmp = new ArrayList<CarAdvertisement>(queryList);
		for(CarAdvertisement car : tmp)
		{
			if(parameters.getPriceFrom()>0)
				if(parameters.getPriceFrom()>car.getPrice())
					queryList.remove(car);
			if(parameters.getPriceTo()>0)
				if(parameters.getPriceTo()<car.getPrice())
					queryList.remove(car);
		}
		
	}

}
