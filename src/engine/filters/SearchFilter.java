package engine.filters;

import java.util.ArrayList;

import model.CarAdvertisement;
import engine.CarSearchParameters;

public interface SearchFilter {

	CarSearchParameters getParameters();
	void setParameters(CarSearchParameters parameters);
	boolean canApplyFilter();
	void applyFilter(ArrayList<CarAdvertisement> queryList);
	
}
